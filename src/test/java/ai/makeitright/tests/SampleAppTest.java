package ai.makeitright.tests;

import ai.makeitright.utils.DriverConfig;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

public class SampleAppTest extends DriverConfig {

    private static final Logger logger = Logger.getLogger(SampleAppTest.class.getName());

    @Test
    public void checkText() throws IOException {
        logger.info("testng - SampleAppTest.checkText +7");

        String screenshotFilePath = System.getProperty("SCREENSHOTS_PATH");
        String artifactFilePath = System.getProperty("ARTIFACTS_PATH");

        logger.info("screenshotFilePath=" + screenshotFilePath);
        logger.info("artifactFilePath=" + artifactFilePath);

        File artifact = new File(artifactFilePath + "/artifact.txt");

        if (artifact.createNewFile()) {
            logger.info("File created: " + artifact.getName());
        } else {
            logger.info("File already exists...");
        }

        File screenshot = new File(screenshotFilePath + "/screenshot.txt");

        if (screenshot.createNewFile()) {
            logger.info("screenshot created: " + screenshot.getName());
        } else {
            logger.info("screenshot already exists...");
        }

        Assert.assertEquals("TRUE", "FALSE");

        System.setProperty("output", "{\"result\":{\"out1\":\"val1\"}}");

        logger.info("DONE");

        Assert.assertEquals("TRUE", "TRUE");
    }
}
